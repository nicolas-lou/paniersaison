const controllers = require('../controllers/markerController');

api.get('/markers/:area', controllers.getMarkers);
