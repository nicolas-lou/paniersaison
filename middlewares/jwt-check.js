const jwt = require('../modules/jwt');

module.exports = (req,res,next)=>{    
    jwt.checkToken(req.cookies.token, (err, decoded)=> {
        if(err){ 
            console.log(decoded);           
            res.status(401).json({message: err.message})
        }else{            
            if(!req.user){
                req.payload = decoded;
            }            
            next();
        }    
    })
    
}