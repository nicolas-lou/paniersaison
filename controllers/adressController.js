// controllers/index.js
const AdressService = require('../services/adressService')


exports.findAdress= (req, res) =>{
    AdressService.findAll()
    .then(
      allAdress => {
        res.status(200).json(allAdress);
      }
    )      
  };

  exports.create = (req, res) => {
    AdressService.create(req.body)
        .then(
            (adressCreated) => {
                res.status(201).json(adressCreated);
            }
        )

};


  

