const MarkersService = require('../services/markerService');

exports.getMarkers = (req, res) => {
    MarkersService.getMarkers(req.params.area).then(
        Markers => {
            res.status(200).json(Markers);
        }
    )
};