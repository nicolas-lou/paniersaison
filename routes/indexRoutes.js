const controllers = require('../controllers/adressController')

api.get('/adress', controllers.findAdress);
api.post('/adress', controllers.create);

require('./markerRoutes');