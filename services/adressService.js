const adress = require('../models/adressModel');



exports.create = data => {
    return adress.create({
        strNumber: data.strNumber,
        strName: data.strName,
        zipcode: data.zipcode,
        city: data.city,
        country: data.country,
        farmName: data.farmName,
        lng: data.lng,
        lat:  data.lat
    })
}

exports.findById = id => {
    return adress.findById(id)
}

exports.findAll = ()=> {
    return adress.findAll()
}

exports.delete = id => {
    return adress.destroy({
        where: {id: id}
    })
}
exports.update = (newData, id )=> {
    return adress.update(newData, {where : { id: id}});
}