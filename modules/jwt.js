const jwt = require('jsonwebtoken');
const jwt_secret_key = 'dpyrWB3ZQ43DOGN7mTDGa5JE6KASyZ1UH87uLVQKiLU8v0rQPxqTpltLjItpkJF'
exports.generateToken = (user, callback) => {
    jwt.sign({
     id: user.id,
     company: user.company 
    }, 
    jwt_secret_key, 
    { 
        algorithm: 'HS256',
        expiresIn: 1200
     },
     callback);
}

exports.checkToken = (token,callback ) => {
    jwt.verify(
        token,
        jwt_secret_key,
        callback
    )

}