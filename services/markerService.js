const adress = require('../models/adressModel');

exports.getMarkers = area => {
    return adress.findAll({
        attributes: {exclude: ['result']},
        where:{
            zipcode: {
                [Op.like]: `${area}%`
            }
        }
    })
}