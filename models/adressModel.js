module.exports = sequelize.define('adresses', {
    id_adresse: {
        type: Sequelize.INTEGER,
        field: 'id_adresse',
        allowNull: false,
        primaryKey: true
    },
    num_rue: {
        type: Sequelize.STRING,
        field: 'num_rue',
        allowNull: false    
    },
    city: {
        type: Sequelize.STRING,
        field: 'city',
        allowNull: false
    },
    zipcode: {
        type: Sequelize.STRING,
        field: 'zipcode'
    },
    latitude: {
        type: Sequelize.STRING,
        field: 'latitude'
    },
    longitude: {
        type: Sequelize.STRING,
        field: 'longitude'
    },
    result: {
        type: Sequelize.STRING,
        field: 'result'
    },
    id_ferme: {
        type: Sequelize.INTEGER,
        field: 'id_ferme'
    } 
},
{
    timestamps: false
});