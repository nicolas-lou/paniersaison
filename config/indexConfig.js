let config = null;
const fs = require('fs');
const path = require('path');

function loadFile(fileName){
    if(!fs.existsSync(fileName)){
        throw new Error (`"${fileName}" does not exists`)
    }
    if(config===null){
        config = require(fileName)
    }
    return config;
}

exports.load = ()=> {
    const env = app.get('env');
    console.log(app.get('env'));
   return loadFile(path.join(__dirname,`${env}.json` ))
}
