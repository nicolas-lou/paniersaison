const express = require('express');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const path = require('path');
const config = require('./config/indexConfig');
const cors = require('./middlewares/cors');
const cookieParser = require('cookie-parser');


app = express();
api = express.Router();
conf = config.load();

Sequelize = require('sequelize');
sequelize = new Sequelize(conf.db.default.url, {
    logging: false,
    operatorsAliases: false
});
Op = sequelize.Op;

sequelize.sync({
    force: false
}).then(() => {
    console.log('Database or tables created');
});

app.use('/api', api);

app.use(morgan('common'));
app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors);
app.use(cookieParser());


api.use(cors);
api.use(bodyParser.urlencoded({ extended: true }));
api.use(bodyParser.json());

api.use(cookieParser());

require(path.join(__dirname,'routes/indexRoutes'));


//démarrage du serveur
app.listen(conf.server.port, function () {
  console.log('Panier de Saison écoute sur le port '+ conf.server.port)
})



